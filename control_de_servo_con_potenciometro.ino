#include <Servo.h>

Servo ponce_servo;

const int pinpot = 0;
const int pinservo = 2;
const int pulsomin= 650;
const int pulsomax= 2550;

int valor;
int angulo;

void setup() {
 ponce_servo.attach(pinservo, pulsomin, pulsomax);
}

void loop() {
  valor = analogRead(pinpot);
  angulo = map(valor, 0, 1023, 180, 0);
  ponce_servo.write(angulo);
  delay(20);
}
