# Programas Arduino #

### Estos son unos cuantos script básicos para el uso del Arduino  ###

## Reguiere tener: ##
* IDE arduino
* Placa Arduino UNO

## Instalar el IDE ##
* $ sudo apt-get install arduino
* $ sudo apt-get update

### Luego ###
- Conectar la placa al pc por el pueto USB
- Dentro del IDE, seleccionamos el puerto serial (si no sabemos en cual se conecto, ponemos en un termina   ' $ lsusb ' así pendremos ver en 
cual se conecto )
- Realizar el montaje del circuito con el Arduino y el protoboard que sea necesario