#include <Servo.h>

Servo ale_servo;

const int pin_servo = 2;
const int derecha = 120;
const int izguierda = 120;

int movimiento;


void setup() {
  ale_servo.attach(pin_servo, derecha, izguierda);
}

void loop() {
  movimiento = map(derecha, 0, 1023, izguierda, 0);
  ale_servo.write(movimiento);
  delay(800);
  
  movimiento = map(izguierda, 1023,0 , derecha, 0);
  ale_servo.write(movimiento);
  
  delay(800);
}
