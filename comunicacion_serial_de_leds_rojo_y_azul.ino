const int ledrojo = 6;
const int ledazul = 5;
const int pot_rojo = 0;
const int pot_azul = 1;
int rojo;
int azul;

void setup() {
  Serial.begin(9600);
  pinMode(ledrojo, OUTPUT);
  pinMode(ledazul, OUTPUT);
}

void loop(){
  rojo = analogRead(pot_rojo) /4;
  azul = analogRead(pot_azul) / 4;
  
  analogWrite(ledrojo, rojo);
  analogWrite(ledazul, azul);
  Serial.print("rojo: ");
  Serial.print(rojo);
  Serial.print(",");
  
  Serial.print("azul: ");
  Serial.println(azul);
  
  delay(600);
  
  
}
  
  
