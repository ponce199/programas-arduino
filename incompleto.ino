int estado;
int salida = 0;
int estadoanterior = 0;
int boton_1 = 12;
int boton_2 = 13;
int leds[] = {
  2,3,4,5,6,7};
int i;
int e;
int o;



void setup() {
  for(i = 0; i < leds[i]; i++){
  pinMode(leds[i], OUTPUT);
  }
  pinMode(boton_1, INPUT);
  pinMode(boton_2, INPUT);
}


void loop() {
  
 estado = digitalRead(boton_1);
 
 if((estado == HIGH) && (estadoanterior == LOW)){
   salida = 1 - salida;
   delay(20);
 }
 
 estadoanterior = estado;
 
 if (salida == 1){
   for (e = 0; e < leds[e]; e++){
     digitalWrite(leds[e], HIGH);
   }
 }
 
  estado = digitalRead(boton_2);
 
 if((estado == HIGH) && (estadoanterior == LOW)){
   salida = 1 - salida;
   delay(20);
 }
 
 estadoanterior = estado;
 
 if (salida == 1){
   for( o = 0; o < leds[o]; o--){
     digitalWrite(leds[o], LOW);
   }
 }
}


