const int sensor = 0;
const int ledrojo = 5;
const int ledazul = 6;

long miliVolts;
long temperatura;
int brillo;

void setup() {
  //Serial.begin(9600);
  pinMode(ledrojo, OUTPUT);
  pinMode(ledazul, OUTPUT);
  
  
}

void loop() {
  miliVolts = (analogRead(sensor) * 5000L) /1023;
 temperatura = miliVolts / 10; 
 
 brillo = map(temperatura, 10, 40, 0, 255);
 brillo = constrain(brillo, 0, 255);
 
 analogWrite(ledrojo, brillo);
 analogWrite(ledazul, 255 - brillo);
 
 
/* Serial.print("temperatura: ");
 Serial.print(temperatura);
 Serial.print(" grados");
 delay(200);*/
}
